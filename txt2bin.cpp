#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>

#include "precondition.hpp"

//---------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    namespace po = boost::program_options;
    po::options_description desc(
            "Converts text adjacency matrix to binary format that is accepted\n"
            "by the rest of the programs.\n\nOptions");
    desc.add_options()
        ("help", "show this help.")
        (
         "input,i",
         po::value<std::string>()->required(),
         "input file in text format"
        )
        (
         "output,o",
         po::value<std::string>()->required(),
         "output file in binary format"
        )
        (
         "header,h",
         po::bool_switch()->default_value(false),
         "Input file has header (two lines with number of nodes and number of links)"
        )
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    po::notify(vm);

    std::string inp_file = vm["input"].as<std::string>();
    std::string out_file = vm["output"].as<std::string>();
    bool        header   = vm["header"].as<bool>();

    std::ifstream txt(inp_file);
    std::ofstream bin(out_file, std::ios::binary);

    precondition(txt, std::string("Failed to open \"") + inp_file + "\" for reading");
    precondition(bin, std::string("Failed to open \"") + out_file + "\" for writing");

    std::cout << "Converting \"" << inp_file << "\" to \"" << out_file << "\"..." << std::endl;

    size_t n = 0, nnz = 0;
    if (header) {
        precondition(txt >> n >> nnz, "wrong file format");
    }

    bin.write((char*)&n,   sizeof(n));
    bin.write((char*)&nnz, sizeof(nnz));

    int i, j;
    for (n = nnz = 0; txt >> i >> j; ++nnz) {
        n = std::max<size_t>(i--, n);
        n = std::max<size_t>(j--, n);

        bin.write((char*)&i, sizeof(i));
        bin.write((char*)&j, sizeof(j));
    }

    if (!header) {
        bin.seekp(0);
        bin.write((char*)&n,   sizeof(n));
        bin.write((char*)&nnz, sizeof(nnz));
    }

    std::cout << "Done. nodes: " << n << ", links: " << nnz << std::endl;
}
