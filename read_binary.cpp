#include <iostream>
#include <fstream>
#include "read_binary.hpp"

int read_binary(const std::string &fname,
        std::vector<size_t> &ptr, std::vector<int> &col)
{
    std::ifstream f(fname, std::ios::binary);

    size_t n, nnz;
    f.read((char*)&n,   sizeof(n));
    f.read((char*)&nnz, sizeof(nnz));

    std::cout << "Reading \"" << fname << "\"" << std::endl
              << "  nodes: " << n << ", " << "links: " << nnz << std::endl;

    ptr.clear(); ptr.reserve(n+1);
    col.clear(); col.reserve(nnz);

    int i, j, last_row = -1;
    for(size_t k = 0; k < nnz; ++k) {
        f.read((char*)&i, sizeof(i));
        f.read((char*)&j, sizeof(j));

        for(; last_row < i; ++last_row) ptr.push_back(col.size());

        col.push_back(j);
    }
    while(ptr.size() < n + 1) ptr.push_back(col.size());

    return n;
}
