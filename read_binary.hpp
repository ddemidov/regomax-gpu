#ifndef READ_BINARY_HPP
#define READ_BINARY_HPP

#include <string>
#include <vector>

int read_binary(const std::string &fname,
        std::vector<size_t> &ptr, std::vector<int> &col);

#endif
