#!/usr/bin/python

import sys
from pylab import *
from scipy.sparse import csr_matrix

with open(sys.argv[1], 'rb') as f:
    n = fromfile(f, dtype=int32, count=1)[0]
    ptr = fromfile(f, dtype=int64, count=n+1)
    col = fromfile(f, dtype=int32, count=ptr[-1])

A = csr_matrix((ones_like(col), col, ptr), shape=(n,n))
spy(A[-10000:, -10000:], marker='.', markersize=0.5)
show()
