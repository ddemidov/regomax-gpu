#ifndef CUTHILL_MCKEE_HPP
#define CUTHILL_MCKEE_HPP

#include <vector>
#include <algorithm>
#include "precondition.hpp"

template <bool reverse = false>
void cuthill_mckee(
        const std::vector<size_t> &ptr, const std::vector<int> &col,
        std::vector<int> &perm)
{
    const int n = ptr.size() - 1;

    /* The data structure used to sort and traverse the level sets:
     *
     * The current level set is currentLevelSet;
     * In this level set, there are nodes with degrees from 0 (not really
     * useful) to maxDegreeInCurrentLevelSet.
     * firstWithDegree[i] points to a node with degree i, or to -1 if it
     * does not exist. nextSameDegree[firstWithDegree[i]] points to the
     * second node with that degree, etc.
     * While the level set is being traversed, the structure for the next
     * level set is generated; nMDICLS will be the next
     * maxDegreeInCurrentLevelSet and nFirstWithDegree will be
     * firstWithDegree.
     */
    int initialNode = 0; // node to start search
    int maxDegree   = 0;

    std::vector<int> degree(n);
    std::vector<int> levelSet(n, 0);
    std::vector<int> nextSameDegree(n, -1);

#pragma omp parallel
    {
        int maxd = 0;
#pragma omp for
        for(int i = 0; i < n; ++i) {
            degree[i] = ptr[i+1] - ptr[i];
            maxd = std::max(maxd, degree[i]);
        }
#pragma omp critical
        {
            maxDegree = std::max(maxDegree, maxd);
        }
    }

    std::vector<int> firstWithDegree(maxDegree + 1, -1);
    std::vector<int> nFirstWithDegree(maxDegree + 1);

    // Initialize the first level set, made up by initialNode alone
    perm[0] = initialNode;
    int currentLevelSet = 1;
    levelSet[initialNode] = currentLevelSet;
    int maxDegreeInCurrentLevelSet = degree[initialNode];
    firstWithDegree[maxDegreeInCurrentLevelSet] = initialNode;

    // Main loop
    for (int next = 1; next < n; ) {
        int nMDICLS = 0;
        std::fill(nFirstWithDegree.begin(), nFirstWithDegree.end(), -1);
        bool empty = true; // used to detect different connected components

        int firstVal  = reverse ? maxDegreeInCurrentLevelSet : 0;
        int finalVal  = reverse ? -1 : maxDegreeInCurrentLevelSet + 1;
        int increment = reverse ? -1 : 1;

        for(int soughtDegree = firstVal; soughtDegree != finalVal; soughtDegree += increment)
        {
            int node = firstWithDegree[soughtDegree];
            while (node > 0) {
                // Visit neighbors
                for(size_t j = ptr[node], e = ptr[node+1]; j < e; ++j) {
                    int c = col[j];
                    if (levelSet[c] == 0) {
                        levelSet[c] = currentLevelSet + 1;
                        perm[next] = c;
                        ++next;
                        empty = false; // this level set is not empty
                        nextSameDegree[c] = nFirstWithDegree[degree[c]];
                        nFirstWithDegree[degree[c]] = c;
                        nMDICLS = std::max(nMDICLS, degree[c]);
                    }
                }
                node = nextSameDegree[node];
            }
        }

        ++currentLevelSet;
        maxDegreeInCurrentLevelSet = nMDICLS;
        for(int i = 0; i <= nMDICLS; ++i)
            firstWithDegree[i] = nFirstWithDegree[i];

        if (empty) {
            // The graph contains another connected component that we
            // cannot reach.  Search for a node that has not yet been
            // included in a level set, and start exploring from it.
            bool found = false;
            for(int i = 0; i < n; ++i) {
                if (levelSet[i] == 0) {
                    perm[next] = i;
                    ++next;
                    levelSet[i] = currentLevelSet;
                    maxDegreeInCurrentLevelSet = degree[i];
                    firstWithDegree[maxDegreeInCurrentLevelSet] = i;
                    found = true;
                    break;
                }
            }
            precondition(found, "Internal consistency error at skyline_lu");
        }
    }
}

#endif
