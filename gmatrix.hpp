#ifndef GMATRIX_HPP
#define GMATRIX_HPP

#include <vector>
#include <vexcl/vector.hpp>
#include <vexcl/reductor.hpp>

struct gmatrix {
    // Create Google matrix from raw data
    gmatrix(const std::vector<vex::backend::command_queue> &ctx,
            double alpha, int n,
            const std::vector<size_t> &ptr,
            const std::vector<int>    &col,
            const std::vector<double> &val);

    // Create Google matrix from preprocessed data
    gmatrix(const std::vector<vex::backend::command_queue> &ctx,
            int n, int m, int spmv_batch, bool transposed,
            const std::vector<size_t> &ptr,
            const std::vector<int>    &col,
            const std::vector<double> &val,
            const std::vector<double> &rw);

    void mul(const vex::vector<double> &x, vex::vector<double> &y) const;
    void batch_mul(int nv, const vex::vector<double> &x, vex::vector<double> &y) const;

    int n, m, batch_size;
    bool transposed;

    vex::vector<double> row_wgt;
    vex::vector<size_t> ptr;
    vex::vector<int>    col;
    vex::vector<double> val;
    vex::Reductor<double> sum;
};

#endif
