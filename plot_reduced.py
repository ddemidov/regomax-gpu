#!/usr/bin/python

import sys
from pylab import *

def read_matrix(fname):
    with open(fname, 'rb') as f:
        n = fromfile(f, dtype=int32, count=1)[0]
        g = fromfile(f, dtype=float64, count=n*n).reshape((n,n))
        return g

g = sum([read_matrix(f) for f in sys.argv[1:]], axis=0)

print(g)
print('W =', sum(g) / g.shape[0])
imshow(g)
colorbar()
show()
