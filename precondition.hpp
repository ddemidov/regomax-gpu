#ifndef PRECONDITION_HPP
#define PRECONDITION_HPP

#include <iostream>

template <class Cond, class Msg>
void precondition(const Cond &c, const Msg &msg) {
    if (static_cast<bool>(c)) return;

    std::cerr << "Error: " << msg << std::endl;
    exit(1);
}

#endif
