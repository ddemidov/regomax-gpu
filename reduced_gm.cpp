#include <iostream>
#include <iterator>
#include <vexcl/vexcl.hpp>
#include <boost/program_options.hpp>

#include "read_binary.hpp"
#include "sort_columns.hpp"
#include "gmatrix.hpp"
#include "power_iteration.hpp"
#include "projector.hpp"
#include "cuthill_mckee.hpp"

//---------------------------------------------------------------------------
struct rgm_params {
    double alpha   = 0.85;
    double tol     = 1e-12;
    int maxiter    = 100;
    int spmv_batch = 4;
};

//---------------------------------------------------------------------------
void reduced_gm(
        vex::Context &ctx, vex::profiler<> &prof,
        // input
        const std::vector<size_t> &ptr,
        const std::vector<int>    &col,
        const std::vector<double> &val,
        const std::vector<int>    &inc_nodes,
        const rgm_params &prm,
        // output
        std::vector<double> &Grr,
        std::vector<double> &Gpr,
        std::vector<double> &Gqr
        )
{
    int n  = ptr.size() - 1;
    int nr = inc_nodes.size();
    int ns = n - nr;

    prof.tic_cpu("preprocess");
    Grr.clear(); Grr.resize(nr * nr, (1 - prm.alpha) / n);
    Gpr.clear(); Gpr.resize(nr * nr, 0.0);
    Gqr.clear(); Gqr.resize(nr * nr, 0.0);

    // Index nodes included and excluded from the subset
    std::vector<char> inc(n, 0); // Whether node is included (1) or excluded(0)
    std::vector<int>  idx(n);    // Index of each node in the corresponding node-set

    for(int i = 0; i < nr; ++i) {
        int p = inc_nodes[i];
        inc[p] = 1;
        idx[p] = i;
    }
    for(int i = 0, j = 0; i < n; ++i) {
        if (!inc[i]) idx[i] = j++;
    }

    // Get column and row weights of the Google matrix.
    std::vector<double> cs(n), rw(n, (1 - prm.alpha) / n), rw_r(nr), rw_s(ns);

#pragma omp parallel for
    for(int i = 0; i < n; ++i) {
        size_t beg = ptr[i];
        size_t end = ptr[i+1];

        if (beg == end) {
            // Dangling node.
            rw[i] = 1.0 / n;
        }

        double s = 0.0;
        for(size_t j = beg; j < end; ++j) s += val[j];
        cs[i] = s;

        if (inc[i]) {
            rw_r[idx[i]] = rw[i];
        } else {
            rw_s[idx[i]] = rw[i];
        }
    }

    // Split the Google matrix four-ways:
    //   G = | Grr Grs |
    //       | Gsr Gss |
    // Grr goes directly to the output.
    // Grs needs to be transposed.
    // Gsr we keep non-transposed to have easy access to its columns.
    // Gss is non-transposed for now (we need it to find psi_L).
    for(int i = 0, k = 0; i < nr; ++i) {
        for(int j = 0; j < nr; ++j, ++k) {
            Grr[k] = rw[j];
        }
    }
    std::vector<size_t> ptr_rs(nr + 1, 0);
    std::vector<size_t> ptr_sr(ns + 1, 0);
    std::vector<size_t> ptr_ss(ns + 1, 0);
    for(int i = 0; i < n; ++i) {
        size_t beg = ptr[i];
        size_t end = ptr[i+1];

        for(size_t k = beg; k < end; ++k) {
            int j = col[k];

            if (inc[j]) {
                if (inc[i]) { // Grr
                    Grr[idx[j] * nr + idx[i]] += prm.alpha * val[k] / cs[i];
                } else {      // Grs
                    ++ptr_rs[idx[j]+1];
                }
            } else {
                if (inc[i]) { // Gsr
                    ++ptr_sr[idx[i]+1];
                } else {      // Gss
                    ++ptr_ss[idx[i]+1];
                }
            }
        }
    }

    std::partial_sum(ptr_rs.begin(), ptr_rs.end(), ptr_rs.begin());
    std::partial_sum(ptr_sr.begin(), ptr_sr.end(), ptr_sr.begin());
    std::partial_sum(ptr_ss.begin(), ptr_ss.end(), ptr_ss.begin());

    std::vector<int> col_rs(ptr_rs.back());
    std::vector<int> col_sr(ptr_sr.back());
    std::vector<int> col_ss(ptr_ss.back());

    std::vector<double> val_rs(ptr_rs.back());
    std::vector<double> val_sr(ptr_sr.back());
    std::vector<double> val_ss(ptr_ss.back());

    for(int i = 0; i < n; ++i) {
        size_t beg = ptr[i];
        size_t end = ptr[i+1];

        for(size_t k = beg; k < end; ++k) {
            int    j = col[k];
            double v = prm.alpha * val[k] / cs[i];

            if (inc[j]) {
                if (inc[i]) { // Grr
                    continue;
                } else {      // Grs
                    size_t head = ptr_rs[idx[j]]++;
                    col_rs[head] = idx[i];
                    val_rs[head] = v;
                }
            } else {
                if (inc[i]) { // Gsr
                    size_t head = ptr_sr[idx[i]]++;
                    col_sr[head] = idx[j];
                    val_sr[head] = v;
                } else {      // Gss
                    size_t head = ptr_ss[idx[i]]++;
                    col_ss[head] = idx[j];
                    val_ss[head] = v;
                }
            }
        }
    }

    std::rotate(ptr_rs.begin(), ptr_rs.end() - 1, ptr_rs.end()); ptr_rs[0] = 0;
    std::rotate(ptr_sr.begin(), ptr_sr.end() - 1, ptr_sr.end()); ptr_sr[0] = 0;
    std::rotate(ptr_ss.begin(), ptr_ss.end() - 1, ptr_ss.end()); ptr_ss[0] = 0;

    sort_columns(ptr_rs, col_rs, val_rs);
    sort_columns(ptr_sr, col_sr, val_sr);
    sort_columns(ptr_ss, col_ss, val_ss);
    prof.toc("preprocess");


    // Find psi_L (left eigenvector of Gss).
    prof.tic_cl("psi{L}");
    auto Gss = std::make_shared<gmatrix>(ctx, ns, ns, 1, /*transposed=*/true,
            ptr_ss, col_ss, val_ss, rw_s);

    vex::vector<double> psi_L(ctx, ns);
    psi_L = 1.0 / ns;

    std::cout << "Computing psi{L}:" << std::endl;
    double lambda_c = power_iteration(*Gss, prm.maxiter, prm.tol, psi_L);
    std::cout << "lambda_c = " << lambda_c << std::endl;
    prof.toc("psi{L}");

    // Transpose Gss.
    prof.tic_cpu("transpose Gss");
    std::fill(ptr_ss.begin(), ptr_ss.end(), 0);
    for(int i = 0; i < n; ++i) {
        size_t beg = ptr[i];
        size_t end = ptr[i+1];

        for(size_t k = beg; k < end; ++k) {
            int j = col[k];

            if (!inc[i] && ! inc[j])
                ++ptr_ss[idx[j]+1];
        }
    }

    std::partial_sum(ptr_ss.begin(), ptr_ss.end(), ptr_ss.begin());

    for(int i = 0; i < n; ++i) {
        size_t beg = ptr[i];
        size_t end = ptr[i+1];

        for(size_t k = beg; k < end; ++k) {
            int j = col[k];

            if (!inc[i] && !inc[j]) {
                size_t head = ptr_ss[idx[j]]++;
                col_ss[head] = idx[i];
                val_ss[head] = prm.alpha * val[k] / cs[i];
            }
        }
    }

    std::rotate(ptr_ss.begin(), ptr_ss.end() - 1, ptr_ss.end()); ptr_ss[0] = 0;
    sort_columns(ptr_ss, col_ss, val_ss);
    prof.toc("transpose Gss");

    Gss = std::make_shared<gmatrix>(ctx, ns, ns, prm.spmv_batch,
            /*transposed=*/false, ptr_ss, col_ss, val_ss, rw_s);

    // Find psi_R (right eigenvector of Gss).
    prof.tic_cl("psi{R}");
    vex::vector<double> psi_R(ctx, ns);
    psi_R = 1.0 / ns;

    std::cout << "Computing psi{R}" << std::endl;
    lambda_c = power_iteration(*Gss, prm.maxiter, prm.tol, psi_R);
    std::cout << "lambda_c = " << lambda_c << std::endl;
    prof.toc("psi{R}");

    vex::Reductor<double, vex::SUM> sum(ctx);
    vex::Reductor<double, vex::MAX> max(ctx);
    psi_L /= sum(psi_R * psi_L);

    gmatrix Grs(ctx, nr, ns, prm.spmv_batch, /*transposed=*/false,
            ptr_rs, col_rs, val_rs, rw_s);

    projector proj(ctx, psi_L, psi_R);

    // Find
    //   Gpr = Grs P Gsr / (1 - lambda_c)
    // and
    //   Gqr = Grs Q sum(Q Gss Q) Gsr
    vex::vector<double> v(ctx, ns * prm.spmv_batch);
    vex::vector<double> w(ctx, ns * prm.spmv_batch);
    vex::vector<double> u(ctx, ns * prm.spmv_batch);
    vex::vector<double> t(ctx, nr * prm.spmv_batch);

    v = 0;
    w = 0;
    u = 0;

    prof.tic_cl("GR");
    for(int i0 = 0; i0 < nr; i0 += prm.spmv_batch) {
        int i1 = std::min(i0 + prm.spmv_batch, nr);
        int nv = i1 - i0;

        for(int i = i0, k = 0; i < i1; ++i, ++k) {
            vex::permutation(vex::element_index(k * ns, ns))(v) = rw_r[i];
        }

        {
            auto vp = v.map(0);
            for(int i = i0, k = 0; i < i1; ++i, ++k) {
                for(size_t j = ptr_sr[i]; j < ptr_sr[i+1]; ++j) {
                    vp[k * ns + col_sr[j]] += val_sr[j];
                }
            }
        }

        // Gpr
        prof.tic_cl("Gpr");
        proj.batch_mulp(nv, v, w);
        Grs.batch_mul(nv, w, t);
        t *= 1 / (1 - lambda_c);
        
        {
            auto tp = t.map(0);
            for(int i = i0, k = 0; i < i1; ++i, ++k) {
                for (int j = 0; j < nr; ++j) {
                    Gpr[j * nr + i] = tp[k * nr + j];
                }
            }
        }
        prof.toc("Gpr");

        // Gqr
        prof.tic_cl("Gqr");
        w = v;
        for(int k = 0; k < prm.maxiter; ++k) {
            // w = Q Gss Q w
            proj.batch_mulq(nv, w, u);
            Gss->batch_mul(nv, u, w);
            proj.batch_mulq(nv, w, u);
            w.swap(u);

            v += w;

            if (k % 10 == 0) {
                double res = max(abs(w));
                std::cout
                    << "Compute Gqr[" << std::setw(3) << i0 << ":" << std::setw(3) << i1-1 << "]: "
                    << std::setw(3) << k << ": " << res << "\r" << std::flush;
                if (res < prm.tol) break;
            }
        }
        std::cout << std::endl;

        proj.batch_mulq(nv, v, w);
        Grs.batch_mul(nv, w, t);

        {
            auto tp = t.map(0);
            for(int i = i0, k = 0; i < i1; ++i, ++k) {
                for (int j = 0; j < nr; ++j) {
                    Gqr[j * nr + i] = tp[k * nr + j];
                }
            }
        }
        prof.toc("Gqr");
    }
    prof.toc("GR");
}


//---------------------------------------------------------------------------
void save_matrix(const std::string &fname, int n, const std::vector<double> &g) {
    std::ofstream f(fname, std::ios::binary);
    f.write((char*)&n, sizeof(int));
    f.write((char*)g.data(), g.size() * sizeof(double));
}

//---------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    namespace po = boost::program_options;
    po::options_description desc(
            "Computes reduced google matrix as three components (Grr, Gpr, Gqr).\n"
            "The results are saved to binary files 'Grr.bin', 'Gpr.bin', and 'Gqr.bin'.\n"
            "Each file contains size of the reduced matrix n as 4-byte integer,\n"
            "followed by n x n double values arranged row by row.\n"
            "\nOptions");
    desc.add_options()
        ("help,h", "show this help.")
        (
         "input,i",
         po::value<std::string>()->required(),
         "input file in binary format"
        )
        (
         "subset,r",
         po::value<std::string>()->required(),
         "text file with list of node numbers included into the reduced matrix"
        )
        (
         "alpha,a",
         po::value<double>()->default_value(0.85, "0.85"),
         "damping factor"
        )
        (
         "maxiter,m",
         po::value<int>()->default_value(100),
         "maximum number of iterations"
        )
        (
         "tol,t",
         po::value<double>()->default_value(1e-12, "1e-12"),
         "convergence threshold (|p{k+1} - p{k}| < tol)"
        )
        (
         "spmv_batch,b",
         po::value<int>()->default_value(4),
         "batch size for computing Gqr columns"
        )
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    po::notify(vm);

    std::string mtx_file = vm["input"].as<std::string>();
    std::string set_file = vm["subset"].as<std::string>();

    rgm_params prm;
    prm.alpha      = vm["alpha"].as<double>();
    prm.spmv_batch = vm["spmv_batch"].as<int>();
    prm.maxiter    = vm["maxiter"].as<int>();
    prm.tol        = vm["tol"].as<double>();

    vex::Context ctx(vex::Filter::Env && vex::Filter::Count(1));
    std::cout << ctx << std::endl;
    vex::profiler<> prof(ctx);

    prof.tic_cpu("reading");
    std::vector<size_t> ptr;
    std::vector<int>    col;
    read_binary(mtx_file, ptr, col);
    sort_columns(ptr, col);
    std::vector<double> val(ptr.back(), 1.0);
    prof.toc("reading");

    prof.tic_cpu("reordering");
    int n = ptr.size() - 1;
    std::vector<int> perm(n);
    std::vector<int> invp(n);

#if 0
    {
        // save adjacency matrix.
        std::ofstream f("Graw.bin", std::ios::binary);
        f.write((char*)&n, sizeof(int));
        f.write((char*)ptr.data(), ptr.size() * sizeof(ptr[0]));
        f.write((char*)col.data(), col.size() * sizeof(col[0]));
    }
#endif

    {
        prof.tic_cpu("transpose");
        size_t nnz = ptr.back();
        std::vector<size_t> ptr_t(n+1, 0);
        std::vector<int>    col_t(nnz);

        for(size_t j = 0; j < nnz; ++j) ++ptr_t[col[j]+1];

        std::partial_sum(ptr_t.begin(), ptr_t.end(), ptr_t.begin());

        for(int i = 0; i < n; ++i) {
            for(size_t j = ptr[i], e = ptr[i+1]; j < e; ++j) {
                col_t[ptr_t[col[j]]++] = i;
            }
        }

        std::rotate(ptr_t.begin(), ptr_t.end() - 1, ptr_t.end()); ptr_t[0] = 0;
        sort_columns(ptr_t, col_t);
        prof.toc("transpose");

        prof.tic_cpu("cuthill-mckee");
        cuthill_mckee(ptr_t, col_t, perm);
        prof.toc("cuthill-mckee");
    }

    {
        std::vector<size_t> rptr(n+1, 0);
        std::vector<int>    rcol(ptr.back());
        std::vector<double> rval(ptr.back());

#pragma omp parallel for
        for(int i = 0; i < n; ++i) {
            int ii = perm[i];
            invp[ii] = i;
            rptr[i+1] = ptr[ii+1] - ptr[ii];
        }

        std::partial_sum(rptr.begin(), rptr.end(), rptr.begin());

#pragma omp parallel for
        for(int i = 0; i < n; ++i) {
            int ii = perm[i];
            size_t head = rptr[i];
            for(size_t j = ptr[ii], e = ptr[ii+1]; j < e; ++j) {
                rcol[head] = invp[col[j]];
                rval[head] = val[j];
                ++head;
            }
        }

        ptr.swap(rptr);
        col.swap(rcol);
        val.swap(rval);
    }
    prof.toc("reordering");

#if 0
    {
        // save renumbered adjacency matrix
        std::ofstream f("Gperm.bin", std::ios::binary);
        f.write((char*)&n, sizeof(int));
        f.write((char*)ptr.data(), ptr.size() * sizeof(ptr[0]));
        f.write((char*)col.data(), col.size() * sizeof(col[0]));
    }
#endif

    std::vector<int> inc_nodes;
    {
        std::ifstream f(set_file);
        inc_nodes.assign(std::istream_iterator<int>(f), std::istream_iterator<int>());
        int m = inc_nodes.size();
        for(int i = 0; i < m; ++i) inc_nodes[i] = invp[inc_nodes[i]];
    }

    std::vector<double> Grr, Gpr, Gqr;
    prof.tic_cpu("reduced matrix");
    reduced_gm(ctx, prof, ptr, col, val, inc_nodes, prm, Grr, Gpr, Gqr);
    prof.toc("reduced matrix");

    prof.tic_cpu("saving");
    int nr = inc_nodes.size();
    save_matrix("Grr.bin", nr, Grr);
    save_matrix("Gpr.bin", nr, Gpr);
    save_matrix("Gqr.bin", nr, Gqr);
    prof.toc("saving");

    std::cout << prof << std::endl;
}
