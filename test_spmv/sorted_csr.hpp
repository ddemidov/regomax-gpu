#ifndef SORTED_CSR_HPP
#define SORTED_CSR_HPP

#include <vector>
#include <algorithm>
#include <vexcl/vexcl.hpp>

struct sorted_csr_matrix {
    sorted_csr_matrix(const std::vector<vex::backend::command_queue> &ctx,
            int n, const std::vector<size_t> &ptr, const std::vector<int> &col)
        : ctx(ctx), gpu_idx(ctx, n), gpu_ptr(ctx, ptr), gpu_col(ctx, col)
    {
        std::vector<int> idx(n);
        for(int i = 0; i < n; ++i) idx[i] = i;
        std::stable_sort(idx.begin(), idx.end(), [&ptr](int i, int j) {
                int wi = ptr[i+1] - ptr[i];
                int wj = ptr[j+1] - ptr[j];
                return wi < wj;
                });
        vex::copy(idx, gpu_idx);
    }

    // y = A * x
    void mul(const vex::vector<double> &x, vex::vector<double> &y) {
        VEX_FUNCTION(void, csr, (size_t, k)(int*, idx)(size_t*, ptr)(int*, col)(double*, x)(double*, y),
            int i = idx[k];
            double sum = 0.0;
            size_t beg = ptr[i], end = ptr[i+1];
            double v = (beg == end) ? 0.0 : 1.0 / (end - beg);
            for(size_t j = beg; j < end; ++j)
                sum += x[col[j]];
            y[i] = sum * v;
        );
        vex::eval(csr(vex::element_index(), raw_pointer(gpu_idx), raw_pointer(gpu_ptr), raw_pointer(gpu_col), raw_pointer(x), raw_pointer(y)),
                ctx, y.partition());
    }

    const std::vector<vex::backend::command_queue> &ctx;
    vex::vector<int>    gpu_idx;
    vex::vector<size_t> gpu_ptr;
    vex::vector<int>    gpu_col;
};

#endif
