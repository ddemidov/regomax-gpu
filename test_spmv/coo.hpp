#ifndef COO_HPP
#define COO_HPP

#include <vector>
#include <vexcl/vexcl.hpp>

struct coo_matrix {
    coo_matrix(const std::vector<vex::backend::command_queue> &ctx,
            int chunk, int n, const std::vector<size_t> &ptr, const std::vector<int> &col)
        : ctx(ctx), chunk(chunk), n(n), nnz(ptr.back()), coo_row(ctx, nnz), coo_col(ctx, col), d(ctx, n)
    {
        std::vector<int>    row(nnz);
        std::vector<double> inw(n);
        for(int i = 0; i < n; ++i) {
            size_t beg = ptr[i];
            size_t end = ptr[i+1];
            inw[i] = (end == beg) ? 0.0 : 1.0 / (end - beg);
            for(size_t j = beg; j < end; ++j) row[j] = i;
        }

        vex::copy(row, coo_row);
        vex::copy(inw, d);

        coo = vex::backend::kernel(ctx[0], R"(
#if __CUDA_ARCH__ < 600
__device__ double atomicAdd(double* address, double val)
{
    unsigned long long int* address_as_ull =
                              (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;

    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed,
                        __double_as_longlong(val +
                               __longlong_as_double(assumed)));

    } while (assumed != old);

    return __longlong_as_double(old);
}
#endif

extern "C" __global__ void coo(int n, size_t nnz, int chunk,
    int *row, int *col, double *x, double *y)
{
    ulong grid_pos = chunk * (blockDim.x * blockIdx.x + threadIdx.x);
    ulong grid_dim = chunk * (blockDim.x * gridDim.x);
    for(; grid_pos < nnz; grid_pos += grid_dim) {
        double sum;
        int last_i;
        for(ulong idx = grid_pos; idx < grid_pos + chunk && idx < nnz; ++idx) {
            int i = row[idx];
            int j = col[idx];

            if (idx == grid_pos) {
                sum = 0.0;
            } else if (i != last_i) {
                atomicAdd(y + last_i, sum);
                sum = 0.0;
            }

            last_i = i;
            sum += x[j];
        }
        atomicAdd(y + last_i, sum);
    }
}
)", "coo");
    }

    // y = A * x
    void mul(const vex::vector<double> &x, vex::vector<double> &y) {
        y = 0.0;
        coo(ctx[0], n, nnz, chunk, coo_row(0), coo_col(0), x(0), y(0));
        y *= d;
    }

    const std::vector<vex::backend::command_queue> &ctx;
    int chunk, n;
    size_t nnz;

    vex::vector<int>    coo_row;
    vex::vector<int>    coo_col;
    vex::vector<double> d;

    vex::backend::kernel coo;
};

#endif
