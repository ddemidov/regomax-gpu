#include <iostream>
#include <fstream>
#include <vector>
#include <omp.h>

#include <vexcl/vexcl.hpp>

#include "precondition.hpp"
#include "read_binary.hpp"
#include "csr.hpp"
#include "hybell.hpp"
#include "coo.hpp"
#include "sorted_csr.hpp"

//---------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " <in: bin>" << std::endl;
        return 1;
    }

    vex::Context ctx(vex::Filter::Env && vex::Filter::Count(1));
    std::cout << ctx << std::endl;
    std::cout << "OMP cores: " << omp_get_max_threads() << std::endl;

    vex::profiler<> prof(ctx);

    prof.tic_cpu("reading");
    std::vector<size_t> ptr;
    std::vector<int>    col;
    int n = read_binary(argv[1], ptr, col);
    prof.toc("reading");

    std::vector<double> p0(n, 1.0 / n), p1(n);

    for(int k = 0; k < 10; ++k) {
        prof.tic_cpu("spmv (CPU)");
#pragma omp parallel for
        for(int i = 0; i < n; ++i) {
            size_t beg = ptr[i];
            size_t end = ptr[i+1];
            double v = (beg == end) ? 0.0 : 1.0 / (end - beg);
            double sum = 0.0;
            for(size_t j = beg; j < end; ++j)
                sum += v * p0[col[j]];
            p1[i] = sum;
        }
        prof.toc("spmv (CPU)");
        p0.swap(p1);
    }

    std::cout << std::scientific;
    for(int i = 0; i < 10; ++i) std::cout << p0[i] << " ";
    std::cout << std::endl;

    vex::vector<double> gpu_p0(ctx, n), gpu_p1(ctx, n);
    vex::vector<size_t> gpu_ptr(ctx, ptr);
    vex::vector<int>    gpu_col(ctx, col);

    // Naive CSR kernel on the GPU
    {
        csr_matrix A(ctx, n, ptr, col);

        gpu_p0 = 1.0 / n;
        A.mul(gpu_p0, gpu_p1);

        for(int k = 0; k < 10; ++k) {
            prof.tic_cl("spmv (GPU/CSR)");
            A.mul(gpu_p0, gpu_p1);
            prof.toc("spmv (GPU/CSR)");
            gpu_p0.swap(gpu_p1);
        }

        std::cout << std::scientific;
        for(int i = 0; i < 10; ++i) std::cout << gpu_p0[i] << " ";
        std::cout << std::endl;
    }

    // Hybrid ELL kernel.
    {
        hybell_matrix A(ctx, n, ptr, col);
        gpu_p0 = 1.0 / n;
        A.mul(gpu_p0, gpu_p1);

        for(int k = 0; k < 10; ++k) {
            prof.tic_cl("spmv (GPU/HybELL)");
            A.mul(gpu_p0, gpu_p1);
            prof.toc("spmv (GPU/HybELL)");
            gpu_p0.swap(gpu_p1);
        }

        std::cout << std::scientific;
        for(int i = 0; i < 10; ++i) std::cout << gpu_p0[i] << " ";
        std::cout << std::endl;
    }

    // COO kernel.
    for(auto chunk : {16, 32, 64}) {
        std::ostringstream header;
        header << "spmv (GPU/COO[" << chunk << "])";
        coo_matrix A(ctx, chunk, n, ptr, col);
        gpu_p0 = 1.0 / n;
        A.mul(gpu_p0, gpu_p1);

        for(int k = 0; k < 10; ++k) {
            prof.tic_cl(header.str());
            A.mul(gpu_p0, gpu_p1);
            prof.toc(header.str());
            gpu_p0.swap(gpu_p1);
        }

        std::cout << std::scientific;
        for(int i = 0; i < 10; ++i) std::cout << gpu_p0[i] << " ";
        std::cout << std::endl;
    }

    // Sorted CSR kernel on the GPU
    {
        sorted_csr_matrix A(ctx, n, ptr, col);

        gpu_p0 = 1.0 / n;
        A.mul(gpu_p0, gpu_p1);

        for(int k = 0; k < 10; ++k) {
            prof.tic_cl("spmv (GPU/Sorted CSR)");
            A.mul(gpu_p0, gpu_p1);
            prof.toc("spmv (GPU/Sorted CSR)");
            gpu_p0.swap(gpu_p1);
        }
    }

    std::cout << std::scientific;
    for(int i = 0; i < 10; ++i) std::cout << gpu_p0[i] << " ";
    std::cout << std::endl;

    std::cout << prof << std::endl;
}
