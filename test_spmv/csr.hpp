#ifndef CSR_HPP
#define CSR_HPP

#include <vector>
#include <vexcl/vexcl.hpp>

struct csr_matrix {
    csr_matrix(const std::vector<vex::backend::command_queue> &ctx,
            int n, const std::vector<size_t> &ptr, const std::vector<int> &col)
        : ctx(ctx), ptr(ctx, ptr), col(ctx, col)
    {
    }

    // y = A * x
    void mul(const vex::vector<double> &x, vex::vector<double> &y) {
        VEX_FUNCTION(double, csr, (size_t, i)(size_t*, ptr)(int*, col)(double*, x),
            double sum = 0.0;
            size_t beg = ptr[i], end = ptr[i+1];
            for(size_t j = beg; j < end; ++j)
                sum += x[col[j]];
            return sum;
        );
        y = csr(vex::element_index(), raw_pointer(ptr), raw_pointer(col), raw_pointer(x));
    }

    const std::vector<vex::backend::command_queue> &ctx;
    vex::vector<size_t> ptr;
    vex::vector<int>    col;
};

#endif
