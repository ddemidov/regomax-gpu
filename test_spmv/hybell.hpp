#ifndef HYBELL_HPP
#define HYBELL_HPP

#include <vector>
#include <vexcl/vexcl.hpp>

struct hybell_matrix {
    hybell_matrix(const std::vector<vex::backend::command_queue> &ctx,
            int n, const std::vector<size_t> &ptr, const std::vector<int> &col)
        : ctx(ctx), n(n), ell_pitch(alignup(n, 16U)), nnz(ptr.back()), csr_nnz(0)
    {
        /* 1. Get optimal ELL widths for local and remote parts. */
        // Speed of ELL relative to CSR:
        const double ell_vs_csr = 3.0;

        // Find maximum widths for local and remote parts:
        int max_width = 0;
        for(int i = 0; i < n; ++i)
            max_width = std::max<int>(max_width, ptr[i+1] - ptr[i]);

        // Build width distribution histogram.
        std::vector<int> hist(max_width + 1, 0);
        for(int i = 0; i < n; ++i)
            ++hist[ptr[i+1] - ptr[i]];

        // Estimate optimal width for ELL part of the matrix.
        ell_width = max_width;
        {
            int rows = n;
            for(int i = 0; i < max_width; ++i) {
                rows -= hist[i]; // Number of rows wider than i.
                if (ell_vs_csr * rows < n) {
                    ell_width = i;
                    break;
                }
            }
        }

        if (ell_width == 0) {
            csr_nnz = nnz;
            csr_ptr.resize(ctx, ptr);
            csr_col.resize(ctx, col);
            return;
        }

        // Count nonzeros in CSR part of the matrix.
        for(int i = ell_width + 1; i <= max_width; ++i)
            csr_nnz += hist[i] * (i - ell_width);

        /* 3. Split the input matrix into ELL and CSR submatrices. */
        std::vector<int> _ell_col(ell_pitch * ell_width, static_cast<int>(-1));
        std::vector<size_t> _csr_ptr;
        std::vector<int> _csr_col;

        if (csr_nnz) {
            _csr_ptr.resize(n + 1);
            _csr_col.resize(csr_nnz);

            _csr_ptr[0] = 0;
            for(int i = 0; i < n; ++i) {
                int w = ptr[i+1] - ptr[i];
                _csr_ptr[i+1] = _csr_ptr[i] + (w > ell_width ? w - ell_width : 0);
            }
        }


        for(int i = 0; i < n; ++i) {
            int w = 0;
            size_t csr_head = csr_nnz ? _csr_ptr[i] : 0;
            for(size_t j = ptr[i], e = ptr[i+1]; j < e; ++j, ++w) {
                int c = col[j];

                if (w < ell_width) {
                    _ell_col[i + w * ell_pitch] = c;
                } else {
                    _csr_col[csr_head] = c;
                    ++csr_head;
                }
            }
        }

        ell_col.resize(ctx, _ell_col);

        if (csr_nnz) {
            csr_ptr.resize(ctx, _csr_ptr);
            csr_col.resize(ctx, _csr_col);
        }
    }

    // y = A * x
    void mul(const vex::vector<double> &x, vex::vector<double> &y) {
        VEX_FUNCTION(double, ell_only, (int, i)(int, ell_pitch)(int, ell_width)(int*, ell_col)(double*, x),
            double sum = 0;
            int w = 0;
            for(int j = 0; j < ell_width; ++j) {
              int c = ell_col[i + w * ell_pitch];
              if (c == -1) break;
              sum += x[c];
              ++w;
            }
            return sum / w;
        );

        VEX_FUNCTION(double, csr_only, (size_t, i)(size_t*, csr_ptr)(int*, csr_col)(double*, x),
            double sum = 0.0;
            size_t beg = csr_ptr[i], end = csr_ptr[i+1];
            double v = (beg == end) ? 0.0 : 1.0 / (end - beg);
            for(size_t j = beg; j < end; ++j)
                sum += x[csr_col[j]];
            return sum * v;
        );

        VEX_FUNCTION(double, hybell, (int, i)(int, ell_pitch)(int, ell_width)(int*, ell_col)(size_t*, csr_ptr)(int*, csr_col)(double*, x),
            double sum = 0;
            int w = 0;

            for(int j = 0; j < ell_width; ++j) {
              int c = ell_col[i + w * ell_pitch];
              if (c == -1) break;
              sum += x[c];
              ++w;
            }

            size_t beg = csr_ptr[i], end = csr_ptr[i+1];
            for(size_t j = beg; j < end; ++j)
                sum += x[csr_col[j]];

            w += end - beg;
            return sum / (w ? w : 1);
        );

        if (csr_nnz == 0) {
            y = ell_only(vex::element_index(), ell_pitch, ell_width, raw_pointer(ell_col), raw_pointer(x));
        } else if (ell_width == 0) {
            y = csr_only(vex::element_index(), raw_pointer(csr_ptr), raw_pointer(csr_col), raw_pointer(x));
        } else {
            y = hybell(vex::element_index(), ell_pitch, ell_width, raw_pointer(ell_col), raw_pointer(csr_ptr), raw_pointer(csr_col), raw_pointer(x));
        }
    }

    const std::vector<vex::backend::command_queue> &ctx;
    int n, ell_pitch, ell_width;
    size_t nnz;
    size_t csr_nnz;

    vex::vector<int>    ell_col;
    vex::vector<size_t> csr_ptr;
    vex::vector<int>    csr_col;

    static int alignup(int n, int m = 16U) {
        return (n + m - 1) / m * m;
    }
};

#endif
