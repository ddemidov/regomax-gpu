#include <iostream>
#include <fstream>
#include <vector>
#include <vexcl/vexcl.hpp>
#include <boost/program_options.hpp>

#include "precondition.hpp"
#include "read_binary.hpp"
#include "gmatrix.hpp"
#include "power_iteration.hpp"

//---------------------------------------------------------------------------
std::string node_name(const std::string &fname, int n) {
    std::string line;
    std::ifstream f(fname);

    for(int i = 0; std::getline(f, line); ++i)
        if (i == n) return line;

    return "";
}

//---------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    namespace po = boost::program_options;
    po::options_description desc(
            "Computes global (not reduced) PageRank.\n"
            "The output file is in binary format, and contains pagerank size n\n"
            "as 4 byte integer folowed by pagerank vector as n double values.\n"
            "\nOptions");
    desc.add_options()
        ("help,h", "show this help.")
        (
         "input,i",
         po::value<std::string>()->required(),
         "input file in binary format"
        )
        (
         "output,o",
         po::value<std::string>()->required(),
         "output file (unsorted pagerank) in binary format"
        )
        (
         "names,n",
         po::value<std::string>()->default_value(""),
         "file with node names"
        )
        (
         "alpha,a",
         po::value<double>()->default_value(0.85, "0.85"),
         "damping factor"
        )
        (
         "maxiter,m",
         po::value<int>()->default_value(100),
         "maximum number of iterations"
        )
        (
         "tol,t",
         po::value<double>()->default_value(1e-12, "1e-12"),
         "convergence threshold (|p{k+1} - p{k}| < tol)"
        )
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    po::notify(vm);

    std::string inp_file = vm["input"].as<std::string>();
    std::string out_file = vm["output"].as<std::string>();
    std::string names    = vm["names"].as<std::string>();
    double      alpha    = vm["alpha"].as<double>();
    int         maxiter  = vm["maxiter"].as<int>();
    double      tol      = vm["tol"].as<double>();

    vex::Context ctx(vex::Filter::Env && vex::Filter::Count(1));
    std::cout << ctx << std::endl;

    vex::profiler<> prof(ctx);

    prof.tic_cpu("reading");
    std::vector<size_t> ptr;
    std::vector<int>    col;
    int n = read_binary(inp_file, ptr, col);
    std::vector<double> val(ptr.back(), 1.0);
    prof.toc("reading");

    prof.tic_cl("init gmatrix");
    gmatrix G(ctx, alpha, n, ptr, col, val);
    prof.toc("init gmatrix");

    vex::vector<double> p(ctx, n);
    vex::vector<double> t(ctx, n);
    vex::Reductor<double, vex::MAX> max(ctx);

    std::cout << "Computing pagerank..." << std::endl;
    prof.tic_cl("compute PR");
    p = 1.0 / n;
    double lambda = power_iteration(G, maxiter, tol, p);
    std::cout << "lambda = " << lambda << std::endl;
    prof.toc("compute PR");

    prof.tic_cpu("writing");
    {
        auto P = p.map(0);
        std::ofstream f(out_file, std::ios::binary);
        f.write((char*)&n,    sizeof(n));
        f.write((char*)&P[0], sizeof(double) * n);
    }
    prof.toc("writing");

    prof.tic_cl("sort nodes");
    vex::vector<int> rank(ctx, n);
    rank = vex::element_index();
    vex::sort_by_key(p, rank, vex::greater<double>());
    prof.toc("sort nodes");

    std::cout << "\nTop 10 ranked nodes:" << std::endl;
    for(int i = 0; i < 10; ++i) {
        std::cout << std::setw(8) << rank[i] << " - " << p[i];
        if (!names.empty()) std::cout << " - " << node_name(names, rank[i]);
        std::cout << std::endl;
    }

    std::cout << prof << std::endl;
}
