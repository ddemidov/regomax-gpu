GPGPU computation of PageRank [1] and Reduced Google Matrix [2] with VexCL [3].

1. Langville, Amy N., and Carl D. Meyer. Google's PageRank and beyond: The science of search engine rankings. Princeton University Press, 2011.
2. Frahm, K. M., and D. L. Shepelyansky. "Reduced Google matrix." arXiv preprint arXiv:1602.02394 (2016).
3. https://github.com/ddemidov/vexcl

Main files:

* txt2bin.cpp: convert text adjacency matrix to binary format that is accepted
  by the rest of the programs.
  ```
  ./txt2bin --help
  Options:
    --help                show this help.
    -i [ --input ] arg    input file in text format
    -o [ --output ] arg   output file in binary format
    -h [ --header ]       Input file has header (two lines with number of nodes 
                          and number of links)
  ```

* pagerank.cpp: compute global pagerank.
  ```
  ./pagerank --help
  Options:
    -h [ --help ]               show this help.
    -i [ --input ] arg          input file in binary format
    -o [ --output ] arg         output file (unsorted pagerank) in binary format
    -n [ --names ] arg          file with node names
    -a [ --alpha ] arg (=0.85)  damping factor
    -m [ --maxiter ] arg (=100) maximum number of iterations
    -t [ --tol ] arg (=1e-12)   convergence threshold (|p{k+1} - p{k}| < tol)
  ```
  The output file is in binary format, and contains pagerank size n as 4 byte
  integer folowed by pagerank vector as n double values.
  An example of reading the file in python:
  ```
  with open(args.pagerank, 'rb') as f:
      n = numpy.fromfile(f, dtype=numpy.int32, count=1)[0]
      p = numpy.fromfile(f, dtype=numpy.float64, count=n)
  ```

* reduced_gm.cpp: compute reduced google matrix as three components (Grr, Gpr,
  Gqr). The results are saved to binary files "Grr.bin", "Gpr.bin", and
  "Gqr.bin". Each file contains size of the reduced matrix n as 4-byte integer,
  followed by n x n double values arranged row by row.
  ```
  ./reduced_gm --help
  Options:
    -h [ --help ]                show this help.
    -i [ --input ] arg           input file in binary format
    -r [ --subset ] arg          text file with list of node numbers included
                                 into the reduced matrix
    -a [ --alpha ] arg (=0.85)   damping factor
    -m [ --maxiter ] arg (=100)  maximum number of iterations
    -t [ --tol ] arg (=1e-12)    convergence threshold (|p{k+1} - p{k}| < tol)
    -b [ --spmv_batch ] arg (=4) batch size for computing Gqr columns
  ```
* transpose_adj.cpp: transpose binary adjacency matrix so that the output is
  suitable for CheiRank computation.
  ```
  ./transpose_adj --help
  Options:
    -h [ --help ]         show this help.
    -i [ --input ] arg    input file in binary format
    -o [ --output ] arg   output file in binary format
  ```
