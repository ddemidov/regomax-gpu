#ifndef POWER_ITERATION_HPP
#define POWER_ITERATION_HPP

#include <vexcl/vexcl.hpp>

template <class Matrix>
double power_iteration(const Matrix &A, int maxiter, double tol, vex::vector<double> &p) {
    vex::vector<double> t(p.queue_list(), p.size());
    vex::Reductor<double, vex::MAX> max(p.queue_list());
    vex::Reductor<double, vex::SUM> sum(p.queue_list());

    std::cout << std::scientific;
    for(int i = 0; i < maxiter; ++i) {
        A.mul(p, t);
        t /= sum(t);
        p.swap(t);

        if (i % 10 == 0) {
            double res = max(abs(p - t));
            std::cout << "  " << std::setw(3) << i << ": " << res << std::endl;
            if (res < tol) break;
        }
    }

    A.mul(p, t);
    return sum(t * p) / sum(p * p);
}

#endif
