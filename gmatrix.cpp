#include <vexcl/vexcl.hpp>
#include "gmatrix.hpp"

//---------------------------------------------------------------------------
gmatrix::gmatrix(const std::vector<vex::backend::command_queue> &ctx,
        int n, int m, int spmv_batch, bool transposed,
        const std::vector<size_t> &ptr,
        const std::vector<int>    &col,
        const std::vector<double> &val,
        const std::vector<double> &rw)
    : n(n), m(m), transposed(transposed), row_wgt(ctx, rw),
      ptr(ctx, ptr), col(ctx, col), val(ctx, val),
      sum(ctx)
{
}

//---------------------------------------------------------------------------
gmatrix::gmatrix(const std::vector<vex::backend::command_queue> &ctx,
        double alpha, int n,
        const std::vector<size_t> &ptr,
        const std::vector<int>    &col,
        const std::vector<double> &val)
    : n(n), m(n), transposed(false), row_wgt(ctx, n), sum(ctx)
{
    // Compute column and row weights.
    // Keep in mind the input is transposed.
    std::vector<double> rw(n);

    double w1 = 1.0 / n;
    double w2 = (1 - alpha) / n;

#pragma omp parallel for
    for(int i = 0; i < n; ++i) {
        size_t beg = ptr[i];
        size_t end = ptr[i+1];

        rw[i] = (beg == end) ? w1 : w2;
    }

    vex::copy(rw, row_wgt);

    // Prepare transpose of the input matrix.
    size_t nnz = ptr.back();
    std::vector<size_t> t_ptr(n+1, 0);
    for(size_t j = 0; j < nnz; ++j)
        ++t_ptr[col[j]+1];

    // Transpose the input matrix
    std::partial_sum(t_ptr.begin(), t_ptr.end(), t_ptr.begin());
    std::vector<int>    t_col(nnz);
    std::vector<double> t_val(nnz);
    for(int i = 0; i < n; ++i) {
        size_t beg = ptr[i];
        size_t end = ptr[i+1];

        double s = 0.0;
        for(size_t j = beg; j < end; ++j) s += val[j];

        for(size_t j = beg; j < end; ++j) {
            size_t head = t_ptr[col[j]]++;
            t_col[head] = i;
            t_val[head] = alpha * val[j] / s;
        }
    }

    std::rotate(t_ptr.begin(), t_ptr.end() - 1, t_ptr.end());
    t_ptr[0] = 0;

    this->ptr.resize(ctx, t_ptr);
    this->col.resize(ctx, t_col);
    this->val.resize(ctx, t_val);
}

//---------------------------------------------------------------------------
void gmatrix::mul(const vex::vector<double> &x, vex::vector<double> &y) const {
    VEX_FUNCTION(double, spmv, (size_t, i)(size_t*, ptr)(int*, col)(double*, val)(double*, x),
        double sum = 0.0;
        size_t beg = ptr[i], end = ptr[i+1];
        for(size_t j = beg; j < end; ++j)
            sum += val[j] * x[col[j]];
        return sum;
    );

    if (transposed) {
        y = spmv(vex::element_index(), raw_pointer(ptr), raw_pointer(col), raw_pointer(val), raw_pointer(x))
          + row_wgt * sum(x);
    } else {
        y = spmv(vex::element_index(), raw_pointer(ptr), raw_pointer(col), raw_pointer(val), raw_pointer(x))
          + sum(row_wgt * x);
    }
}

//---------------------------------------------------------------------------
void gmatrix::batch_mul(int nv, const vex::vector<double> &x, vex::vector<double> &y) const {
    VEX_FUNCTION(double, spmv, (int, idx)(int, n)(int, m)(size_t*, ptr)(int*, col)(double*, val)(double*, x),
        int i = idx % n;
        int k = idx / n;
        x += k * m;
        double sum = 0.0;
        size_t beg = ptr[i], end = ptr[i+1];
        for(size_t j = beg; j < end; ++j)
            sum += val[j] * x[col[j]];
        return sum;
    );

    if (transposed) {
        y = spmv(vex::element_index(), n, m, raw_pointer(ptr), raw_pointer(col), raw_pointer(val), raw_pointer(x));
        for(int i = 0; i < nv; ++i) {
            auto cur_row = vex::permutation(vex::element_index(i * n, n));
            auto cur_col = vex::permutation(vex::element_index(i * m, m));
            cur_row(y) += row_wgt * sum(cur_col(x));
        }
    } else {
        y = spmv(vex::element_index(), n, m, raw_pointer(ptr), raw_pointer(col), raw_pointer(val), raw_pointer(x));
        for(int i = 0; i < nv; ++i) {
            auto cur_row = vex::permutation(vex::element_index(i * n, n));
            auto cur_col = vex::permutation(vex::element_index(i * m, m));
            cur_row(y) += sum(row_wgt * cur_col(x));
        }
    }
}
