#ifndef PROJECTOR_HPP
#define PROJECTOR_HPP

#include <vexcl/vector.hpp>
#include <vexcl/reductor.hpp>

struct projector {
    projector(const std::vector<vex::backend::command_queue> &ctx,
            const vex::vector<double> &psi_L, const vex::vector<double> &psi_R)
        : n(psi_L.size()), psi_L(psi_L), psi_R(psi_R), sum(ctx)
    {}

    void mulp(const vex::vector<double> &x, vex::vector<double> &y) const {
        y = psi_R * sum(psi_L * x);
    }

    void batch_mulp(int nv, const vex::vector<double> &x, vex::vector<double> &y) const {
        for(int i = 0; i < nv; ++i) {
            auto slice = vex::permutation(vex::element_index(i * n, n));
            slice(y) = psi_R * sum(psi_L * slice(x));
        }
    }

    void mulq(const vex::vector<double> &x, vex::vector<double> &y) const {
        y = x - psi_R * sum(psi_L * x);
    }

    void batch_mulq(int nv, const vex::vector<double> &x, vex::vector<double> &y) const {
        for(int i = 0; i < nv; ++i) {
            auto slice = vex::permutation(vex::element_index(i * n, n));
            slice(y) = slice(x) - psi_R * sum(psi_L * slice(x));
        }
    }

    int n;

    const vex::vector<double> &psi_L;
    const vex::vector<double> &psi_R;
    vex::Reductor<double> sum;
};

#endif
