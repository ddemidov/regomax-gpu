#!/usr/bin/python

import sys
import argparse

from pylab import *

parser = argparse.ArgumentParser(sys.argv[0])
parser.add_argument('-p,--by_rank', dest='rank', help='List of nodes ordered by pagerank')
parser.add_argument('-s,--by_size', dest='size', help='List of nodes ordered naturaly')
args = parser.parse_args(sys.argv[1:])

nodes_by_rank = [l.strip() for l in open(args.rank).readlines()]
nodes_by_size = [l.strip() for l in open(args.size).readlines()]

assert len(nodes_by_size) == len(nodes_by_rank)

n = len(nodes_by_size)
o = list(range(1, n+1))
c = list()

for i in o:
    c.append(len(set(nodes_by_size[:i]) & set(nodes_by_rank[:i])) / i)

plot(o, c)
show()
