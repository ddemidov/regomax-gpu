#include <algorithm>
#include "sort_columns.hpp"

void sort_columns(const std::vector<size_t> &ptr, std::vector<int> &col) {
    int n = ptr.size() - 1;
#pragma omp parallel for
    for(int i = 0 ; i < n; ++i) {
        size_t beg = ptr[i];
        size_t end = ptr[i+1];
        std::sort(col.begin() + beg, col.begin() + end);
    }
}

template <typename Col, typename Val>
void sort_row(Col *col, Val *val, int n) {
    for(int j = 1; j < n; ++j) {
        Col c = col[j];
        Val v = val[j];

        int i = j - 1;

        while(i >= 0 && col[i] > c) {
            col[i + 1] = col[i];
            val[i + 1] = val[i];
            i--;
        }

        col[i + 1] = c;
        val[i + 1] = v;
    }
}

void sort_columns(const std::vector<size_t> &ptr, std::vector<int> &col, std::vector<double> &val) {
    int n = ptr.size() - 1;
#pragma omp parallel for
    for(int i = 0 ; i < n; ++i) {
        size_t beg = ptr[i];
        size_t end = ptr[i+1];
        sort_row(&col[0] + beg, &val[0] + beg, end - beg);
    }
}

