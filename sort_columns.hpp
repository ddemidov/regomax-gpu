#ifndef SORT_COLUMNS_HPP
#define SORT_COLUMNS_HPP

#include <vector>

void sort_columns(const std::vector<size_t> &ptr, std::vector<int> &col);
void sort_columns(const std::vector<size_t> &ptr, std::vector<int> &col, std::vector<double> &val);

#endif
