#!/usr/bin/python

import sys
import argparse

import numpy as np

parser = argparse.ArgumentParser(sys.argv[0])
parser.add_argument('-n,--nodes',  dest='nodes', help='File with node numbers')
parser.add_argument('-t,--titles', dest='titles', help='File with wiki titles')
parser.add_argument('-p,--pagerank', dest='pagerank', help='Binary file with pagerank')
args = parser.parse_args(sys.argv[1:])

titles = open(args.titles).readlines()
nodes = np.loadtxt(args.nodes, dtype=np.int32)

with open(args.pagerank, 'rb') as f:
    n = np.fromfile(f, dtype=np.int32, count=1)[0]
    p = np.fromfile(f, dtype=np.float64, count=n)

for node in sorted(nodes, reverse=True, key=lambda i: p[i]):
    print(titles[node].strip())
