cmake_minimum_required(VERSION 3.9)
if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Build type")
    message(STATUS "No build type selected, default to ${CMAKE_BUILD_TYPE}")
endif()
project(pagerank)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(OpenMP)
find_package(Boost COMPONENTS program_options)
add_subdirectory(vexcl)

add_library(precondition INTERFACE)
target_include_directories(precondition INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})

add_library(read_binary INTERFACE)
target_include_directories(read_binary INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})
target_sources(read_binary INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/read_binary.cpp)

add_library(sort_columns INTERFACE)
target_include_directories(sort_columns INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})
target_sources(sort_columns INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/sort_columns.cpp)

add_executable(pagerank pagerank.cpp gmatrix.cpp)
target_link_libraries(pagerank
    precondition read_binary VexCL::OpenCL OpenMP::OpenMP_CXX Boost::program_options)

add_executable(reduced_gm reduced_gm.cpp gmatrix.cpp)
target_link_libraries(reduced_gm
    precondition read_binary sort_columns VexCL::OpenCL OpenMP::OpenMP_CXX Boost::program_options)

add_executable(txt2bin txt2bin.cpp)
target_link_libraries(txt2bin precondition Boost::program_options)

add_executable(transpose_adj transpose_adj.cpp)
target_link_libraries(transpose_adj sort_columns precondition read_binary Boost::program_options)

foreach(script plot_reduced.py show_matrix.py plot_overlap.py sort_by_pagerank.py)
    configure_file(
        ${CMAKE_CURRENT_SOURCE_DIR}/${script}
        ${CMAKE_CURRENT_BINARY_DIR}/${script}
        COPYONLY)
endforeach()

add_subdirectory(test_spmv)
