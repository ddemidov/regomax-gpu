#include <iostream>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <boost/program_options.hpp>

#include "read_binary.hpp"
#include "sort_columns.hpp"

//---------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    namespace po = boost::program_options;
    po::options_description desc(
            "Transposes binary adjacency matrix so that the output is\n"
            "suitable for CheiRank computation.\n"
            "\nOptions");
    desc.add_options()
        ("help,h", "show this help.")
        (
         "input,i",
         po::value<std::string>()->required(),
         "input file in binary format"
        )
        (
         "output,o",
         po::value<std::string>()->required(),
         "output file in binary format"
        )
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    po::notify(vm);

    std::string inp_file = vm["input"].as<std::string>();
    std::string out_file = vm["output"].as<std::string>();

    // Read
    std::vector<size_t> ptr;
    std::vector<int>    col;
    read_binary(inp_file, ptr, col);
    sort_columns(ptr, col);

    size_t n   = ptr.size() - 1;
    size_t nnz = ptr.back();

    // Transpose
    std::vector<size_t> ptr_t(n+1, 0);
    std::vector<int>    col_t(nnz);

    for(int i = 0; i < n; ++i) {
        for(size_t j = ptr[i], e = ptr[i+1]; j < e; ++j)
            ++ptr_t[col[j]+1];
    }

    std::partial_sum(ptr_t.begin(), ptr_t.end(), ptr_t.begin());

    for(int i = 0; i < n; ++i) {
        for(size_t j = ptr[i], e = ptr[i+1]; j < e; ++j) {
            col_t[ptr_t[col[j]]++] = i;
        }
    }

    std::rotate(ptr_t.begin(), ptr_t.end() - 1, ptr_t.end()); ptr_t[0] = 0;
    sort_columns(ptr_t, col_t);

    // Write
    std::ofstream f(out_file, std::ios::binary);

    f.write((char*)&n, sizeof(n));
    f.write((char*)&nnz, sizeof(nnz));

    for(int i = 0; i < static_cast<int>(n); ++i) {
        for(size_t j = ptr_t[i], e = ptr_t[i+1]; j < e; ++j) {
            int c = col_t[j];
            f.write((char*)&i, sizeof(int));
            f.write((char*)&c, sizeof(int));
        }
    }
}
